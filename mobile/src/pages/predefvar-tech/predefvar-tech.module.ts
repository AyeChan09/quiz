import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PredefvarTechPage } from './predefvar-tech';

@NgModule({
  declarations: [
    PredefvarTechPage,
  ],
  imports: [
    IonicPageModule.forChild(PredefvarTechPage),
  ],
})
export class PredefvarTechPageModule {}
