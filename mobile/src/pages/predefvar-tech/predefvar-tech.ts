import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

/**
 * Generated class for the PredefvarTechPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'PredefvarTechPage'
})
@Component({
  selector: 'page-predefvar-tech',
  templateUrl: 'predefvar-tech.html',
})
export class PredefvarTechPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/predefvartech.json').subscribe(data => {
      this.data = data.data;
      
      this.id = parseInt(localStorage.getItem("predefvartech"));
      
      this.record = this.data[this.id];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PredefvarTechPage');
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("predefvartech", this.id.toString());
    this.navCtrl.setRoot("PredefvarQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

}
