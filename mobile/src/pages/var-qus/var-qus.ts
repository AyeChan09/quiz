import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

@IonicPage({
  name: 'VarQusPage'
})
@Component({
  selector: 'page-var-qus',
  templateUrl: 'var-qus.html',
})
export class VarQusPage {
  data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/varqus.json').subscribe(data => {
        this.data = data.data;
        this.id = parseInt(localStorage.getItem("varqus"));
        this.idTech = parseInt(localStorage.getItem("vartech"));
        this.record = this.data[this.id];
        this.recordCount = parseInt(localStorage.getItem("varresult"));
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VarQusPage');
  }

  goBack() {
    this.idTech--;
    localStorage.setItem("vartech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("varqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("VarTechPage", this.id);
    } else {
      this.navCtrl.setRoot("VarResultPage");
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("varresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
  }

}
