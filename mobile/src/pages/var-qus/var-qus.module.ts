import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VarQusPage } from './var-qus';

@NgModule({
  declarations: [
    VarQusPage,
  ],
  imports: [
    IonicPageModule.forChild(VarQusPage),
  ],
})
export class VarQusPageModule {}
