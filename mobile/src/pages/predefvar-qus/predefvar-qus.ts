import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

/**
 * Generated class for the PredefvarQusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 

@IonicPage({
  name: 'PredefvarQusPage'
})
@Component({
  selector: 'page-predefvar-qus',
  templateUrl: 'predefvar-qus.html',
})
export class PredefvarQusPage {

   data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/predefvarqus.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.id = parseInt(localStorage.getItem("predefvarqus"));
        this.idTech = parseInt(localStorage.getItem("predefvartech"));
        
        this.record = this.data[this.id];
        console.log('record=>', this.record);
        this.recordCount = parseInt(localStorage.getItem("predefvarresult"));
        console.log('result=>', this.recordCount);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PredefvarQusPage');
  }

  goBack() {
    this.idTech--;
    localStorage.setItem("predefvartech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("predefvarqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("PredefvarTechPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("PredefvarResultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("predefvarresult", this.recordCount.toString());
        console.log('result in getItem=?', localStorage.getItem("predefvarresult"));
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
