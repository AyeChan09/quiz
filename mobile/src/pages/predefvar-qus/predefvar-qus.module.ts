import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PredefvarQusPage } from './predefvar-qus';

@NgModule({
  declarations: [
    PredefvarQusPage,
  ],
  imports: [
    IonicPageModule.forChild(PredefvarQusPage),
  ],
})
export class PredefvarQusPageModule {}
