import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CsQusPage } from './cs-qus';

@NgModule({
  declarations: [
    CsQusPage,
  ],
  imports: [
    IonicPageModule.forChild(CsQusPage),
  ],
})
export class CsQusPageModule {}
