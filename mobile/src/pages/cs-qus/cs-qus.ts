import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

/**
 * Generated class for the CsQusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'CsQusPage'
})
@Component({
  selector: 'page-cs-qus',
  templateUrl: 'cs-qus.html',
})
export class CsQusPage { data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/csqus.json').subscribe(data => {
        this.data = data.data;
        
        this.id = parseInt(localStorage.getItem("csqus"));
        this.idTech = parseInt(localStorage.getItem("cstech"));
        
        this.record = this.data[this.id];
        
        this.recordCount = parseInt(localStorage.getItem("csresult"));
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CsQusPage');
  }

  goBack() {
    this.idTech--;
    localStorage.setItem("cstech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("csqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("CsTechPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("CsResultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("csresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
