import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuncTechPage } from './func-tech';

@NgModule({
  declarations: [
    FuncTechPage,
  ],
  imports: [
    IonicPageModule.forChild(FuncTechPage),
  ],
})
export class FuncTechPageModule {}
