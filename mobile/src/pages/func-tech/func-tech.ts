import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'FuncTechPage'
})
@Component({
  selector: 'page-func-tech',
  templateUrl: 'func-tech.html',
})
export class FuncTechPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/functech.json').subscribe(data => {
      this.data = data.data;
      console.log(this.data);
      this.id = parseInt(localStorage.getItem("functech"));
      
      this.record = this.data[this.id];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuncTechPage');
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("functech", this.id.toString());
    this.navCtrl.setRoot("FuncQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

}
