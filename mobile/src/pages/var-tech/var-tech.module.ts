import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VarTechPage } from './var-tech';

@NgModule({
  declarations: [
    VarTechPage,
  ],
  imports: [
    IonicPageModule.forChild(VarTechPage),
  ],
})
export class VarTechPageModule {}
