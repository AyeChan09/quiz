import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'VarTechPage'
})
@Component({
  selector: 'page-var-tech',
  templateUrl: 'var-tech.html',
})
export class VarTechPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/vartech.json').subscribe(data => {
      this.data = data.data;
      this.id = parseInt(localStorage.getItem("vartech"));
      this.record = this.data[this.id];
    });
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("vartech", this.id.toString());
    this.navCtrl.setRoot("VarQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VarTechPage');
  }

}
