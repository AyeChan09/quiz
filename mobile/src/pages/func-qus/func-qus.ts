import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

@IonicPage({
  name: 'FuncQusPage'
})
@Component({
  selector: 'page-func-qus',
  templateUrl: 'func-qus.html',
})
export class FuncQusPage {

  data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/funcqus.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.id = parseInt(localStorage.getItem("funcqus"));
        this.idTech = parseInt(localStorage.getItem("functech"));
        
        this.record = this.data[this.id];
        
        this.recordCount = parseInt(localStorage.getItem("funcresult"));
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuncQusPage');
  }
  goBack() {
    this.idTech--;
    localStorage.setItem("functech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("funcqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("FuncTechPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("FuncResultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("funcresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
