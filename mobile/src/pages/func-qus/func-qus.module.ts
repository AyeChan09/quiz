import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuncQusPage } from './func-qus';

@NgModule({
  declarations: [
    FuncQusPage,
  ],
  imports: [
    IonicPageModule.forChild(FuncQusPage),
  ],
})
export class FuncQusPageModule {}
