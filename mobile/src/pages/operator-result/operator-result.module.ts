import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OperatorResultPage } from './operator-result';

@NgModule({
  declarations: [
    OperatorResultPage,
  ],
  imports: [
    IonicPageModule.forChild(OperatorResultPage),
  ],
})
export class OperatorResultPageModule {}
