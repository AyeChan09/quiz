import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-operator-result',
  templateUrl: 'operator-result.html',
})
export class OperatorResultPage {

  id: number; 
  total: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("operatorresult"));
    this.apiService.getData_local('assets/json/operatorqus.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

 ionViewDidLoad() {
    console.log('ionViewDidLoad OperatorResultPage');
  }

  goHome() {
    localStorage.setItem('array', 'true');
    this.navCtrl.setRoot(HomePage);
  }

}