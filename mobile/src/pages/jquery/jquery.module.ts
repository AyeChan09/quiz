import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JqueryPage } from './jquery';

@NgModule({
  declarations: [
    JqueryPage,
  ],
  imports: [
    IonicPageModule.forChild(JqueryPage),
  ],
})
export class JqueryPageModule {}
