import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Location } from '@angular/common';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'JqueryPage'
})
@Component({
  selector: 'page-jquery',
  templateUrl: 'jquery.html',
})
export class JqueryPage {
  id: number;
  data: any = [];
  // data: any = [{ id: 1, title: 'CH.1:Q.1', question: 'PHP is a...', choice: [
  //   { description: 'Server side programming language', id: 1, },
  //   { description: 'Website', id: 2, },
  //   { description: 'Markup language', id: 3, },
  // ], answer: 'Server side programming language' }, 
  // { id: 2, title: 'CH.1:Q.2', question: 'Can you run PHP on Linux?', choice: [
  //   { description: 'Yes', id: 1, },
  //   { description: 'No', id: 2, },
  //   { description: 'Slightly', id: 3, },
  // ], answer: 'Yes' },
  // { id: 3, title: 'CH.1:Q.3', question: 'Which of the following characters indicates comments in PHP?', choice: [
  //   { description: '***', id: 1, },
  //   { description: '//', id: 2, },
  //   { description: '<!----', id: 3, },
  // ], answer: '//' }, 
  // { id: 4, title: 'CH.1:Q.2', question: 'What does SPL stand for?', choice: [
  //   { description: 'Source PHP Library', id: 1, },
  //   { description: 'Standard PHP Library', id: 2, },
  //   { description: 'Source PHP LIst', id: 3, },
  // ], answer: 'Source PHP Library' }, 
  // { id: 5, title: 'CH.1:Q.2', question: 'Which of the following is true about php.ini file?', choice: [
  //   { description: 'The PHP configuration file, php.ini, is the final and most immediate way to affect PHPs functionality', id: 1, },
  //   { description: 'The php.ini file is read each time PHP is initialized.', id: 2, },
  //   { description: 'Both of the above.', id: 3, },
  // ], answer: 'Both of the above.' }, 
  // { id: 6, title: 'CH.1:Q.2', question: 'Which of the following type of variables are special variables that hold references to resources external to PHP (such as database connections)?', choice: [
  //   { description: 'Arrays', id: 1, },
  //   { description: 'Objects', id: 2, },
  //   { description: 'Resources', id: 3, },
  // ], answer: 'Resources' }, 
  // { id: 7, title: 'CH.1:Q.2', question: 'Which of the following keyword terminates the for loop or switch statement and transfers execution to the statement immediately following the for loop or switch?', choice: [
  //   { description: 'break', id: 1, },
  //   { description: 'continue', id: 2, },
  //   { description: 'terminate', id: 3, },
  // ], answer: 'break' }, 
  // { id: 8, title: 'CH.1:Q.2', question: 'Which of the following function opens a file?', choice: [
  //   { description: 'fopen()', id: 1, },
  //   { description: 'fread()', id: 2, },
  //   { description: 'filesize()', id: 3, },
  // ], answer: 'fopen()' }, 
  // { id: 9, title: 'CH.1:Q.2', question: 'Which of the following method returns current date and time?', choice: [
  //   { description: 'gettime()', id: 1, },
  //   { description: 'getdate()', id: 2, },
  //   { description: 'time()', id: 3, },
  // ], answer: 'time()' }, 
  // { id: 10, title: 'CH.1:Q.2', question: 'Which of the following method scopes is not supported by PHP?', choice: [
  //   { description: 'private', id: 1, },
  //   { description: 'final', id: 2, },
  //   { description: 'none', id: 3, },
  // ], answer: 'none' }, 
  // // { id: 11, title: 'CH.1:Q.2', question: 'Which of the following method scopes is not supported by PHP?', choice: [
  // //   { description: 'private', id: 1, },
  // //   { description: 'final', id: 2, },
  // //   { description: 'none', id: 3, },
  // // ], answer: 'none' }, 
  // { id: 11, title: 'CH.1:Q.2', question: 'Which Method scope prevents a methodfrom being overridden by a subclass?', choice: [
  //   { description: 'abstract', id: 1, },
  //   { description: 'protected', id: 2, },
  //   { description: 'final', id: 3, },
  // ], answer: 'final' }];
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public location: Location,
    private apiService: ApiServiceProvider,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/quiz.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.id = parseInt(localStorage.getItem("quiz"));
        this.record = this.data[this.id];
        console.log('record=>', this.record);
        this.recordCount = parseInt(localStorage.getItem("quizresult"));
      });
    // this.id = navParams.data;
    // this.record = this.data[this.id];
    // this.recordCount = parseInt(localStorage.getItem("quiz"));
    // console.log('id=>', this.id);
    // console.log('data=>', this.data[this.id]);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JqueryPage');
  }
  goBack() {
    if(this.id == 0)
      this.navCtrl.setRoot(HomePage);
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("quiz", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("JqueryPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("Jqueryq1Page");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("quizresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
