import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'OperatorTechPage'
})
@Component({
  selector: 'page-operator-tech',
  templateUrl: 'operator-tech.html',
})
export class OperatorTechPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/operatortech.json').subscribe(data => {
      this.data = data.data;
      console.log(this.data);
      this.id = parseInt(localStorage.getItem("operatortech"));
      console.log('id=>', this.id);
      this.record = this.data[this.id];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OperatorTechPage');
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("operatortech", this.id.toString());
    this.navCtrl.setRoot("OperatorQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }


}
