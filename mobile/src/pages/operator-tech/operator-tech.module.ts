import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OperatorTechPage } from './operator-tech';

@NgModule({
  declarations: [
    OperatorTechPage,
  ],
  imports: [
    IonicPageModule.forChild(OperatorTechPage),
  ],
})
export class OperatorTechPageModule {}
