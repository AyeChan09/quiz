import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArrQusPage } from './arr-qus';

@NgModule({
  declarations: [
    ArrQusPage,
  ],
  imports: [
    IonicPageModule.forChild(ArrQusPage),
  ],
})
export class ArrQusPageModule {}
