import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

@IonicPage({
  name: 'ArrQusPage'
})
@Component({
  selector: 'page-arr-qus',
  templateUrl: 'arr-qus.html',
})
export class ArrQusPage { data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/arrayqus.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.id = parseInt(localStorage.getItem("arrayqus"));
        this.idTech = parseInt(localStorage.getItem("arraytech"));
        
        this.record = this.data[this.id];
        
        this.recordCount = parseInt(localStorage.getItem("arrayresult"));
      });
  }

 ionViewDidLoad() {
    console.log('ionViewDidLoad ArrQusPage');
  }
  goBack() {
    this.idTech--;
    localStorage.setItem("arraytech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("arrayqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("ArrTechPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("ArrResultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("arrayresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
