import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage({
  name: 'CsResultPage'
})
@Component({
  selector: 'page-cs-result',
  templateUrl: 'cs-result.html',
})
export class CsResultPage {
  id: number; 
  total: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("csresult"));
    this.apiService.getData_local('assets/json/csqus.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CsResultPage');
  }

  goHome() {
    localStorage.setItem('function', 'true');
    this.navCtrl.setRoot(HomePage);
  }

}
