import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CsResultPage } from './cs-result';

@NgModule({
  declarations: [
    CsResultPage,
  ],
  imports: [
    IonicPageModule.forChild(CsResultPage),
  ],
})
export class CsResultPageModule {}
