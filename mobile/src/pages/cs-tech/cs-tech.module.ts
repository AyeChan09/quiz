import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CsTechPage } from './cs-tech';

@NgModule({
  declarations: [
    CsTechPage,
  ],
  imports: [
    IonicPageModule.forChild(CsTechPage),
  ],
})
export class CsTechPageModule {}
