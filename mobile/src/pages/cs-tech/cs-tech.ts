import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'CsTechPage'
})
@Component({
  selector: 'page-cs-tech',
  templateUrl: 'cs-tech.html',
})
export class CsTechPage {

 data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/cstech.json').subscribe(data => {
      this.data = data.data;
      
      this.id = parseInt(localStorage.getItem("cstech"));
      
      this.record = this.data[this.id];
    });
  }

 ionViewDidLoad() {
    console.log('ionViewDidLoad CsTechPage');
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("cstech", this.id.toString());
    this.navCtrl.setRoot("CsQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

}
