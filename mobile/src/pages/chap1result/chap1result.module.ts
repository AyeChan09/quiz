import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chap1resultPage } from './chap1result';

@NgModule({
  declarations: [
    Chap1resultPage,
  ],
  imports: [
    IonicPageModule.forChild(Chap1resultPage),
  ],
})
export class Chap1resultPageModule {}
