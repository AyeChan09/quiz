import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage({
  name: 'Chap1resultPage'
})
@Component({
  selector: 'page-chap1result',
  templateUrl: 'chap1result.html',
})
export class Chap1resultPage {
  id: number; 
  total: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("chap1result"));
    this.apiService.getData_local('assets/json/chap1ques.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Chap1resultPage');
  }

  goHome() {
    localStorage.setItem('variable', 'true');
    this.navCtrl.setRoot(HomePage);
  }

}
