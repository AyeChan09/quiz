import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { JqueryPage } from '../jquery/jquery';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    // localStorage.setItem('variable', 'false');
    // localStorage.setItem('operator', 'false');
    // localStorage.setItem('array', 'false');
    // localStorage.setItem('cs', 'false');
    // localStorage.setItem('function', 'false');
    // localStorage.setItem('predefvar', 'false');
    // localStorage.setItem('quiz', 'false');
  }

  goChapter1TechPage() {
    localStorage.setItem("chap1result", '0');
    localStorage.setItem("chap1", '0');
    localStorage.setItem("chap1tech", '0');
    this.navCtrl.push("Chap1techPage");
  }

  goVariablesPage() {
    let check = localStorage.getItem('variable');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("varresult", '0');
      localStorage.setItem("varqus", '0');
      localStorage.setItem("vartech", '0');
      this.navCtrl.push("VarTechPage");
    }
  }

  goOperatorsPage() {
    let check = localStorage.getItem('operator');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("operatorresult", '0');
      localStorage.setItem("operatorqus", '0');
      localStorage.setItem("operatortech", '0');
      this.navCtrl.push("OperatorTechPage");
    }
  }

  goArraysPage() {
    let check = localStorage.getItem('array');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("arrayresult", '0');
      localStorage.setItem("arrayqus", '0');
      localStorage.setItem("arraytech", '0');
      this.navCtrl.push("ArrTechPage");
    }
  }

  goCSPage() {
    let check = localStorage.getItem('cs');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("csresult", '0');
      localStorage.setItem("csqus", '0');
      localStorage.setItem("cstech", '0');
      this.navCtrl.push("CsTechPage");
    }
  }

  goFunctionsPage() {
    let check = localStorage.getItem('function');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("funcresult", '0');
      localStorage.setItem("funcqus", '0');
      localStorage.setItem("functech", '0');
      this.navCtrl.push("FuncTechPage");
    }
  }

  goPreDefVarPage() {
    let check = localStorage.getItem('predefvar');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("predefvarresult", '0');
      localStorage.setItem("predefvarqus", '0');
      localStorage.setItem("predefvartech", '0');
      this.navCtrl.push("PredefvarTechPage");
    }
  }

  goJQueryPage() {
    let check = localStorage.getItem('quiz');
    console.log('check', check);
    if(check == 'true') {
      localStorage.setItem("quizresult", '0');
      localStorage.setItem("quiz", '0');
      let id = 0;
      this.navCtrl.push("JqueryPage", id);
    }
  }

}
