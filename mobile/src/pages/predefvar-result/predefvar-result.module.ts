import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PredefvarResultPage } from './predefvar-result';

@NgModule({
  declarations: [
    PredefvarResultPage,
  ],
  imports: [
    IonicPageModule.forChild(PredefvarResultPage),
  ],
})
export class PredefvarResultPageModule {}
