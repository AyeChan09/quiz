import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage({
  name: 'PredefvarResultPage'
})
@Component({
  selector: 'page-predefvar-result',
  templateUrl: 'predefvar-result.html',
})
export class PredefvarResultPage  {
  id: number; 
  total: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("predefvarresult"));
    console.log('record=>', this.record);
    this.apiService.getData_local('assets/json/predefvarqus.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PredefvarResultPage');
  }

  goHome() {
    localStorage.setItem('quiz', 'true');
    this.navCtrl.setRoot(HomePage);
  }

}
