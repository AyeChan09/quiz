import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

@IonicPage({
  name: 'OperatorQusPage'
})
@Component({
  selector: 'page-operator-qus',
  templateUrl: 'operator-qus.html',
})
export class OperatorQusPage {
  data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/operatorqus.json').subscribe(data => {
        this.data = data.data;
        this.id = parseInt(localStorage.getItem("operatorqus"));
        this.idTech = parseInt(localStorage.getItem("operatortech"));
        this.record = this.data[this.id];
        this.recordCount = parseInt(localStorage.getItem("operatorresult"));
        console.log('qusID=>', this.id, 'techID=>', this.idTech);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OperatorQusPage');
  }

  goBack() {
    this.idTech--;
    localStorage.setItem("operatortech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("operatorqus", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("OperatorTechPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("OperatorResultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("operatorresult", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
