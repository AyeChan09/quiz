import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OperatorQusPage } from './operator-qus';

@NgModule({
  declarations: [
    OperatorQusPage,
  ],
  imports: [
    IonicPageModule.forChild(OperatorQusPage),
  ],
})
export class OperatorQusPageModule {}
