import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuncResultPage } from './func-result';

@NgModule({
  declarations: [
    FuncResultPage,
  ],
  imports: [
    IonicPageModule.forChild(FuncResultPage),
  ],
})
export class FuncResultPageModule {}
