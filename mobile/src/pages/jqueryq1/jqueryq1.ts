import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage({
  name: 'Jqueryq1Page'
})
@Component({
  selector: 'page-jqueryq1',
  templateUrl: 'jqueryq1.html',
})
export class Jqueryq1Page {
  id: number; 
  total: number = 11;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("quizresult"));
    this.apiService.getData_local('assets/json/quiz.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Jqueryq1Page');
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }



}
