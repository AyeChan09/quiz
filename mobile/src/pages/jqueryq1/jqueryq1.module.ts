import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Jqueryq1Page } from './jqueryq1';

@NgModule({
  declarations: [
    Jqueryq1Page,
  ],
  imports: [
    IonicPageModule.forChild(Jqueryq1Page),
  ],
})
export class Jqueryq1PageModule {}
