import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArrResultPage } from './arr-result';

@NgModule({
  declarations: [
    ArrResultPage,
  ],
  imports: [
    IonicPageModule.forChild(ArrResultPage),
  ],
})
export class ArrResultPageModule {}
