import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VarResultPage } from './var-result';

@NgModule({
  declarations: [
    VarResultPage,
  ],
  imports: [
    IonicPageModule.forChild(VarResultPage),
  ],
})
export class VarResultPageModule {}
