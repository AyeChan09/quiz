import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage({
  name: 'VarResultPage'
})
@Component({
  selector: 'page-var-result',
  templateUrl: 'var-result.html',
})
export class VarResultPage {
  id: number; 
  total: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,) {
    this.record = parseInt(localStorage.getItem("varresult"));
    this.apiService.getData_local('assets/json/varqus.json').subscribe(data => {
      let dataJson = data.data;
      console.log(dataJson);
      this.total = dataJson.length;
      console.log('length=>', this.total);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VarResultPage');
  }

  goHome() {
    localStorage.setItem('operator', 'true');
    this.navCtrl.setRoot(HomePage);
  }

}
