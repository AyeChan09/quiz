import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chap1qPage } from './chap1q';

@NgModule({
  declarations: [
    Chap1qPage,
  ],
  imports: [
    IonicPageModule.forChild(Chap1qPage),
  ],
})
export class Chap1qPageModule {}
