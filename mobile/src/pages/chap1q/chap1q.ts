import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';

@IonicPage({
  name: 'Chap1qPage'
})
@Component({
  selector: 'page-chap1q',
  templateUrl: 'chap1q.html',
})
export class Chap1qPage {
  data: any = [];
  id: number;
  record: any = {};
  recordCount: number;
  isCorrect: boolean = false;
  count: number = 0;
  idTech: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location,
    public toastCtrl: ToastController) {
      this.apiService.getData_local('assets/json/chap1ques.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.id = parseInt(localStorage.getItem("chap1"));
        this.idTech = parseInt(localStorage.getItem("chap1tech"));
        console.log('chap1=>', this.id);
        this.record = this.data[this.id];
        console.log('record=>', this.record);
        this.recordCount = parseInt(localStorage.getItem("chap1result"));
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Chap1qPage');
  }

  goBack() {
    this.idTech--;
    localStorage.setItem("chap1tech", this.idTech.toString());
    this.location.back();
  }
  goNextPage() {
    this.id++;
    localStorage.setItem("chap1", this.id.toString());
    if(this.id < this.data.length) {
      this.navCtrl.setRoot("Chap1techPage", this.id);
      console.log('id=>', this.id, 'question');
    } else {
      this.navCtrl.setRoot("Chap1resultPage");
      console.log('id=>', this.id, 'result');
    }    
  }
  select(item) {    
    if(item.description == this.record.answer) {
      this.isCorrect = true;
      if(this.count == 0) {
        this.recordCount++;
        localStorage.setItem("chap1result", this.recordCount.toString());
      }
      const toast = this.toastCtrl.create({
        message: 'Correct',
        duration: 3000,
        position: "top",
        cssClass: 'success'
      });
      toast.present();
    } else {
      this.isCorrect = false;
      console.log(item.description);
      const toast = this.toastCtrl.create({
        message: 'Wrong',
        duration: 3000,
        position: "top",
        cssClass: 'danger'
      });
      toast.present();
    }
    this.count++;
    console.log("record=>", this.record);
  }

}
