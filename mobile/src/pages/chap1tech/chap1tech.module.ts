import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chap1techPage } from './chap1tech';

@NgModule({
  declarations: [
    Chap1techPage,
  ],
  imports: [
    IonicPageModule.forChild(Chap1techPage),
  ],
})
export class Chap1techPageModule {}
