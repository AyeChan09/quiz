import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'Chap1techPage'
})
@Component({
  selector: 'page-chap1tech',
  templateUrl: 'chap1tech.html',
})
export class Chap1techPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/chap1tech.json').subscribe(data => {
      this.data = data.data;
      
      this.id = parseInt(localStorage.getItem("chap1tech"));
      
      this.record = this.data[this.id];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Chap1techPage');
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("chap1tech", this.id.toString());
    this.navCtrl.setRoot("Chap1qPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

}
