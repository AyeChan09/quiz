import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Location } from '@angular/common';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'ArrTechPage'
})
@Component({
  selector: 'page-arr-tech',
  templateUrl: 'arr-tech.html',
})
export class ArrTechPage {
  data: any = [];
  id: number;
  record: any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public location: Location) {
    this.apiService.getData_local('assets/json/arraytech.json').subscribe(data => {
      this.data = data.data;
      console.log(this.data);
      this.id = parseInt(localStorage.getItem("arraytech"));
      
      this.record = this.data[this.id];
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArrTechPage');
  }

  goNextPage() {
    this.id++;
    localStorage.setItem("arraytech", this.id.toString());
    this.navCtrl.setRoot("ArrQusPage");
  }

  goBack() {
    if(this.id == 0) 
      this.navCtrl.setRoot(HomePage);
    else 
      this.location.back();
  }

}
