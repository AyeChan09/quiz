import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArrTechPage } from './arr-tech';

@NgModule({
  declarations: [
    ArrTechPage,
  ],
  imports: [
    IonicPageModule.forChild(ArrTechPage),
  ],
})
export class ArrTechPageModule {}
