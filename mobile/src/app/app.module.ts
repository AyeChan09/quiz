import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { ApiServiceProvider } from '../providers/api-service/api-service';
import { ConstantProvider } from '../providers/constant/constant';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';


// import { HtmlPage } from '../pages/html/html';
// import { CssPage } from '../pages/css/css';
// import { JavascriptPage } from '../pages/javascript/javascript';
// import { JqueryPage } from '../pages/jquery/jquery';
// import { Htmlq1Page } from '../pages/htmlq1/htmlq1';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    // HtmlPage,
    // CssPage,
    // JavascriptPage,
    // JqueryPage,
    // Htmlq1Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    // HtmlPage,
    // CssPage,
    // JavascriptPage,
    // JqueryPage,
    // Htmlq1Page
  ],
  providers: [
    ApiServiceProvider,
    ConstantProvider,
    HttpModule,
    HttpClientModule,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
